﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using System.IO;

namespace MyFirstApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Constructor
        //class constructor
        public MainWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region MainMethod

        //Logic for BrowseButton, this is an event handler.
        //private method defines the behavior when the BrowseButton is clicked.
        //RoutedEventArgs: contains state info and event data associated with a routed event.
        private async void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            var openDlg = new Microsoft.Win32.OpenFileDialog();

            openDlg.Filter = "JPEG Image(*.jpg)|*.jpg";
            bool? result = openDlg.ShowDialog(this);

            if (!(bool)result)
            {
                return;
            }

            string filePath = openDlg.FileName;

            Uri fileUri = new Uri(filePath);
            BitmapImage bitmapSource = new BitmapImage();

            bitmapSource.BeginInit();
            bitmapSource.CacheOption = BitmapCacheOption.None;
            bitmapSource.UriSource = fileUri;
            bitmapSource.EndInit();

            FacePhoto.Source = bitmapSource; // link Image in xaml to bitmapSource

            Title = "Detecting...";
            FaceRectangle[] faceRects = await UploadAndDetectFaces(filePath);
            Title = String.Format("Detection Finished. {0} face(s) detected", faceRects.Length);

            
            //The following draws the face rectangles.
            if (faceRects.Length > 0)
            {
                DrawingVisual visual = new DrawingVisual();
                DrawingContext drawingContext = visual.RenderOpen();
                drawingContext.DrawImage(bitmapSource,
                new Rect(0, 0, bitmapSource.Width, bitmapSource.Height));
                double dpi = bitmapSource.DpiX;
                double resizeFactor = 96 / dpi;

                foreach (var faceRect in faceRects)
                {
                    drawingContext.DrawRectangle(
                        Brushes.Transparent,
                        new Pen(Brushes.Red, 2),
                        new Rect(
                            faceRect.Left * resizeFactor,
                            faceRect.Top * resizeFactor,
                            faceRect.Width * resizeFactor,
                            faceRect.Height * resizeFactor
                            )
                    );
                }

                drawingContext.Close();
                RenderTargetBitmap faceWithRectBitmap = new RenderTargetBitmap(
                    (int)(bitmapSource.PixelWidth * resizeFactor),
                    (int)(bitmapSource.PixelHeight * resizeFactor),
                    96,
                    96,
                    PixelFormats.Pbgra32);

                faceWithRectBitmap.Render(visual);
                FacePhoto.Source = faceWithRectBitmap;  
                
            }
        }

        #endregion

        #region FaceAPIHelperMethod
        //FaceAPI setup
        private readonly IFaceServiceClient faceServiceClient = new FaceServiceClient("949eab8370f54a6e907786e6c143b78e");

        //Helper method to upload Images to detect faces 
         private async Task<FaceRectangle[]> UploadAndDetectFaces(string imageFilePath)
         {
             try
             {
                 using (Stream imageFileStream = File.OpenRead(imageFilePath))
                 {
                    //The following returns the face attributes.

                    var requiredFaceAttributes = new FaceAttributeType[] {
                    FaceAttributeType.Age,
                    FaceAttributeType.Gender,

                   };

                    var faces = await faceServiceClient.DetectAsync(imageFileStream,
                    returnFaceLandmarks: true,
                    returnFaceAttributes: requiredFaceAttributes);

                    
                    //Set the textBox under different scenarios. 
                    if (faces.Length > 1)
                    {
                        textBox.Text = "Only one person \n is allowed.";
                       
                    }

                    else if (faces.Length == 0)
                    {
                        textBox.Text = "No face \n detected.";

                    }

                    //If only one face is detected, display the age and gender in the textBox.
                    else {
                        foreach (var face in faces)
                        {
                            var id = face.FaceId;
                            var attributes = face.FaceAttributes;
                            var age = attributes.Age;
                            var gender = attributes.Gender;
                            //String Sattributes = ToString (a)
                            string ageAndGender = age.ToString() + " \n" + gender.ToString();
                            textBox.Text = ageAndGender;
                        }
                    }

                    //This method returns an array of face rectangles. 
                     var faceRects = faces.Select(face => face.FaceRectangle);
                     return faceRects.ToArray();
                 }
             }
             catch (Exception)
             {
                 return new FaceRectangle[0];
             }
         }
         #endregion
    }
}
